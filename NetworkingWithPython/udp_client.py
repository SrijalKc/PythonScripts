import socket

def start_client(server_host='127.0.0.1', server_port=65432):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        while True:
            message = input("Client: ")
            s.sendto(message.encode(), (server_host, server_port))
            data, _ = s.recvfrom(1024)  # Buffer size is 1024 bytes
            print(f"Server: {data.decode()}")

if __name__ == "__main__":
    start_client()
